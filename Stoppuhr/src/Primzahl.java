
public class Primzahl {
	
	
	public boolean isPrim(long x) {
	    if (x <= 2)
	        return true;
	    for (long i = 2; i <= x/2; i++)
	        if (x % i == 0)
	            return false;
	    return true;
	}
}
