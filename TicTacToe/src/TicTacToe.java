import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TicTacToe extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TicTacToe frame = new TicTacToe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TicTacToe() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(3, 3, 0, 0));
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton_1.getText().equals("")){
					btnNewButton_1.setText("O");
				}
				else if (btnNewButton_1.getText().equals("O")){
					btnNewButton_1.setText("X");
				}
				else{
					btnNewButton_1.setText("");
				}
			}
		});
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton_3.getText().equals("")){
					btnNewButton_3.setText("O");
				}
				else if (btnNewButton_3.getText().equals("O")){
					btnNewButton_3.setText("X");
				}
				else{
					btnNewButton_3.setText("");
				}
			}
		});
		contentPane.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton_4.getText().equals("")){
					btnNewButton_4.setText("O");
				}
				else if (btnNewButton_4.getText().equals("O")){
					btnNewButton_4.setText("X");
				}
				else{
					btnNewButton_4.setText("");
				}
			}
		});
		contentPane.add(btnNewButton_4);
		
		JButton btnNewButton_2 = new JButton("");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton_2.getText().equals("")){
					btnNewButton_2.setText("O");
				}
				else if (btnNewButton_2.getText().equals("O")){
					btnNewButton_2.setText("X");
				}
				else{
					btnNewButton_2.setText("");
				}
			}
		});
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton.getText().equals("")){
					btnNewButton.setText("O");
				}
				else if (btnNewButton.getText().equals("O")){
					btnNewButton.setText("X");
				}
				else{
					btnNewButton.setText("");
				}
			}
		});
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_6 = new JButton("");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton_6.getText().equals("")){
					btnNewButton_6.setText("O");
				}
				else if (btnNewButton_6.getText().equals("O")){
					btnNewButton_6.setText("X");
				}
				else{
					btnNewButton_6.setText("");
				}
			}
		});
		contentPane.add(btnNewButton_6);
		
		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton_5.getText().equals("")){
					btnNewButton_5.setText("O");
				}
				else if (btnNewButton_5.getText().equals("O")){
					btnNewButton_5.setText("X");
				}
				else{
					btnNewButton_5.setText("");
				}
			}
		});
		contentPane.add(btnNewButton_5);
		
		JButton btnNewButton_7 = new JButton("");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (btnNewButton_7.getText().equals("")){
					btnNewButton_7.setText("O");
				}
				else if (btnNewButton_7.getText().equals("O")){
					btnNewButton_7.setText("X");
				}
				else{
					btnNewButton_7.setText("");
				}
					}
		});
		contentPane.add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("");
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton_8.getText().equals("")){
					btnNewButton_8.setText("O");
				}
				else if (btnNewButton_8.getText().equals("O")){
					btnNewButton_8.setText("X");
				}
				else{
					btnNewButton_8.setText("");
				}
			}
		});
		contentPane.add(btnNewButton_8);
		
	}

}
