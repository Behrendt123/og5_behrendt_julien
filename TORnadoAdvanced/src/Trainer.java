
public class Trainer extends Personen {
	private String lizensklasse;
	private int aufwandentsch�digung;
	
	public Trainer(){}
	
	public Trainer(String name, int telefonnummer, boolean jahresbeitrag,String lizensklasse, int aufwandentsch�digung){
		super(name,telefonnummer,jahresbeitrag);
		this.lizensklasse=lizensklasse;
		this.aufwandentsch�digung=aufwandentsch�digung;
	}

	public String getLizensklasse() {
		return lizensklasse;
	}

	public void setLizensklasse(String lizensklasse) {
		this.lizensklasse = lizensklasse;
	}

	public int getAufwandentsch�digung() {
		return aufwandentsch�digung;
	}

	public void setAufwandentsch�digung(int aufwandentsch�digung) {
		this.aufwandentsch�digung = aufwandentsch�digung;
	}
}
