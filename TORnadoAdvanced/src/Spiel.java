
public class Spiel {
	// Anfang Attribute
	  private boolean heimMannschaft;
	  private String datum;
	  private int heimTore;
	  private int gastTore;
	  private String[] gelbeKarte;
	  private String[] roteKarte;
	  private Schiedsrichter schiedsrichter;
	  // Ende Attribute
	  
	  public Spiel() {
	    this.heimMannschaft = false;
	    this.datum = "";
	    this.heimTore = 0;
	    this.gastTore = 0;
	  }

	  public Spiel(boolean heimMannschaft, String datum, int heimTore, int gastTore, String[] gelbeKarte, String[] roteKarte,Schiedsrichter schiedsrichter) {
	    this.heimMannschaft = heimMannschaft;
	    this.datum =datum;
	    this.heimTore = heimTore;
	    this.gastTore = gastTore;
	    this.gelbeKarte = gelbeKarte;
	    this.roteKarte = roteKarte;
	  }

	  // Anfang Methoden
	  public boolean getHeimMannschaft() {
	    return heimMannschaft;
	  }

	  public void setHeimMannschaft(boolean heimMannschaft) {
	    this.heimMannschaft = heimMannschaft;
	  }

	  public String getDatum() {
	    return datum;
	  }

	  public void setDatum(String datum) {
	    this.datum = datum;
	  }

	  public int getHeimTore() {
	    return heimTore;
	  }

	  public void setHeimTore(int heimTore) {
	    this.heimTore = heimTore;
	  }

	  public int getGastTore() {
	    return gastTore;
	  }

	  public void setGastTore(int gastTore) {
	    this.gastTore = gastTore;
	  }

	  public String[] getGelbeKarte() {
	    return gelbeKarte;
	  }

	  public void setGelbeKarteAn(String[] gelbeKarte) {
	    this.gelbeKarte = gelbeKarte;
	  }

	  public String[] getRoteKarte() {
	    return roteKarte;
	  }

	  public void setRoteKarteAn(String[] roteKarteAn) {
	    this.roteKarte = roteKarte;
	  }

	  

	  public Schiedsrichter getSchiedsrichter() {
	    return schiedsrichter;
	  }

	  public void setSchiedsrichter(Schiedsrichter schiedsrichter) {
	    this.schiedsrichter = schiedsrichter;
	  }
}
