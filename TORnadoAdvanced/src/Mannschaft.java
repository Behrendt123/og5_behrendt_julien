
public class Mannschaft {
	
	  private int anzahlspieler;
	  private Mannschaftsleiter mannschaftsleiter;
	  private Trainer trainer;
	  private String spielklasse;
	  private Spieler[] spieler;
	  private Spiel[] spiel;
	  // Ende Attribute
	  
	  // Anfang Methoden
	  public Mannschaft() {
	    this.anzahlspieler=0;
	    this.mannschaftsleiter = mannschaftsleiter;
	    this.spielklasse = spielklasse;
	  }
	  
	  
	  public Mannschaft(int anzahlspieler, Mannschaftsleiter mannschaftsleiter,Trainer trainer ,String spielklasse,Spieler[] spieler,Spiel[] spiel) {
	    this.setAnzahlspieler(anzahlspieler);
	    this.mannschaftsleiter = mannschaftsleiter;
	    this.trainer = trainer;
	    this.spielklasse = spielklasse;
	    this.spieler = spieler;
	    this.setSpiel(spiel);
	  }
	  
	 
	  
	  public Mannschaftsleiter getLeiter() {
	    return mannschaftsleiter;
	  }
	  
	  
	  public void setLeiter(Mannschaftsleiter leiter) {
	    this.mannschaftsleiter = mannschaftsleiter;
	  }
	  
	  
	  public Trainer getTrainer() {
	    return trainer;
	  }
	  
	  
	  public void setTrainer(Trainer trainer) {
	    this.trainer = trainer;
	  }
	  
	  
	  public String getSpielklasse() {
	    return spielklasse;
	  }
	  
	  public void setSpielklasse(String spielklasse) {
	    this.spielklasse = spielklasse;
	  }
	  
	  public Spieler[] getSpieler() {
	    return spieler;
	  }
	  
	  public void setSpieler(Spieler[] spieler) {
	    this.spieler = spieler;
	  }
	  
	


	public int getAnzahlspieler() {
		return anzahlspieler;
	}


	public void setAnzahlspieler(int anzahlspieler) {
		this.anzahlspieler = anzahlspieler;
	}


	public Spiel[] getSpiel() {
		return spiel;
	}


	public void setSpiel(Spiel[] spiel) {
		this.spiel = spiel;
	}
}
