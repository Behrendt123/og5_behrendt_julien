
public class Spieler extends Personen {
	private int trikotnummer;
	private String spielposition;
	private Mannschaft mannschaft;
	
	public Spieler(){}
	
	public Spieler(String name, int telefonnummer, boolean jahresbeitrag,int trikotnummer, String spielposition,Mannschaft mannschaft){
		super(name,telefonnummer,jahresbeitrag);
		this.spielposition= spielposition;
		this.trikotnummer= trikotnummer;
		this.mannschaft= mannschaft;
	}

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public String getSpielposition() {
		return spielposition;
	}

	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}

	public Mannschaft getMannschaft() {
		return mannschaft;
	}

	public void setMannschaft(Mannschaft mannschaft) {
		this.mannschaft = mannschaft;
	}
}
