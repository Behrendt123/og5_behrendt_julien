
public class Mannschaftsleiter extends Spieler {
	private Mannschaft mannschaft;
	private int engagement;
	
	public Mannschaftsleiter(){}
	
	public Mannschaftsleiter(String name, int telefonnummer, boolean jahresbeitrag,int trikotnummer, String spielposition,Mannschaft mannschaft,int engagement){
		super();
		this.mannschaft= mannschaft;
		this.engagement = engagement;
	}

	public Mannschaft getMannschaft() {
		return mannschaft;
	}

	public void setMannschaft(Mannschaft mannschaft) {
		this.mannschaft = mannschaft;
	}

	public int getEngagement() {
		return engagement;
	}

	public void setEngagement(int engagement) {
		this.engagement = engagement;
	}
	
	public int Rabattbekommen(int engagement){
		int Rabatt= engagement/2;
		return Rabatt;
	}

}
