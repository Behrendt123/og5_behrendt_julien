
public class Schiedsrichter extends Personen{
	private int anzahlSpiele;
	
	public Schiedsrichter(){}
	
	public Schiedsrichter(String name, int telefonnummer, boolean jahresbeitrag, int anzahlSpiele){
		super(name,telefonnummer,jahresbeitrag);
		this.anzahlSpiele=anzahlSpiele;
		
	}

	public int getAnzahlSpiele() {
		return anzahlSpiele;
	}

	public void setAnzahlSpiele(int anzahlSpiele) {
		this.anzahlSpiele = anzahlSpiele;
	}
}
