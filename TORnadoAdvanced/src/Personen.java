
public abstract class Personen {
 private String name;
 private int telefonnummer;
 private boolean jahresbeitrag;
 
 public Personen(){}
 
 public Personen(String name, int telefonnummer, boolean jahresbeitrag){
	 this.setName(name);
	 this.setTelefonnummer(telefonnummer);
	 this.setJahresbeitrag(jahresbeitrag);
 }

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getTelefonnummer() {
	return telefonnummer;
}

public void setTelefonnummer(int telefonnummer) {
	this.telefonnummer = telefonnummer;
}

public boolean isJahresbeitrag() {
	return jahresbeitrag;
}

public void setJahresbeitrag(boolean jahresbeitrag) {
	this.jahresbeitrag = jahresbeitrag;
}
}
