import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
public class BinäreSuche {

    public void sucheBinär(int[] intArr, int beginn, int ende, int zahl) {
        
        int punkt = beginn + ((ende - beginn) / 2);
        
        if (intArr.length == 0) {
            System.out.println("Der Array ist leer.");
            return;
        }
        
        if (punkt >= intArr.length){
            System.out.println(zahl + " ist nicht im Array vorhanden.");
            return;
        }

        if (zahl > intArr[punkt]) {
            sucheBinär(intArr, punkt + 1, ende, zahl);
        } else if (zahl < intArr[punkt] && beginn != punkt) {
            sucheBinär(intArr, beginn, punkt - 1, zahl);
        } else if(zahl == intArr[punkt]) {
            System.out.println(zahl + "  ist an der Position " + punkt + " vorhanden.");
        } else{
            System.out.println(zahl + " ist nicht im Array vorhanden.");
        }
    }

    public static void main(String[] args) {
    	final int ANZAHL = 20000000;
    	int[] a = getSortedList(ANZAHL);
    	Scanner scanner = new Scanner(System.in);
    	int gesuchteZahl = scanner.nextInt();
    	
        BinäreSuche bs = new BinäreSuche();
        Stoppuhr st1 = new Stoppuhr();
		st1.starten();
        bs.sucheBinär(a, 0, a.length - 1, gesuchteZahl);
        st1.stoppen();
		System.out.println(st1.getDauer());
		
		Stoppuhr st2 = new Stoppuhr();
		st2.starten();
		for(int i = 0; i < a.length; i++)
		      if(a[i] == gesuchteZahl)
		        System.out.println(gesuchteZahl + " ist an der Position " + i + " vorhanden.");
		
		      
		st2.stoppen();
		System.out.println(st2.getDauer());
		
		scanner.close();
    }

	public static int[] getSortedList(int laenge){
		    Random rand = new Random(111111);
		    int[] zahlenliste = new int[laenge];
		    int naechsteZahl = 0;
		    
		    for(int i = 0; i < laenge; i++){
		      naechsteZahl += rand.nextInt(3)+1;
		      zahlenliste[i] = naechsteZahl;
		    }
		    
		    return zahlenliste;
		  }
} 