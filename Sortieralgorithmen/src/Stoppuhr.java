
public class Stoppuhr {
	public long startpunkt;
	public long endpunkt;
	
	public Stoppuhr(){}
	
	public void starten(){
		this.startpunkt= System.currentTimeMillis();
	}
	
	public void stoppen(){
		this.endpunkt=System.currentTimeMillis();
	}
	
	public void reset(){
		this.startpunkt=0;
		this.endpunkt=0;
	}
	
	public long getDauer(){
		long dauer= this.endpunkt-this.startpunkt;
		return dauer;
	}
}
