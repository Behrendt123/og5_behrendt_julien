
public class Selectsort {
	
	public static int[] sortieren(int[] liste) {
		for (int i = 0; i < liste.length - 1; i++) {
			for (int j = i + 1; j < liste.length; j++) {
				if (liste[i] > liste[j]) {
					System.out.println(liste[i] + " und " + liste[j] + " werden vertauscht.");
					int temp = liste[i];
					liste[i] = liste[j];
					liste[j] = temp;
				}
			}
		}

		return liste;
	}
	
	public static void main(String[] args) {
		Stoppuhr s1 = new Stoppuhr();
		int[] unsliste = { 5, 8, 11, 2, 20, 4, 9, 1, 17, 13, 12, 18, 3, 19,10,6,14,7,16};
		s1.starten();
		int[] sortliste = sortieren(unsliste);
		s1.stoppen();
		System.out.println(s1.getDauer() + " Millisekunden hat es gedauert.");
		for (int i = 0; i < sortliste.length; i++) {
			System.out.print(sortliste[i] + ", ");
		}

	}

	
}
