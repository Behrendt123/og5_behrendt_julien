
public class Quicksort {
	
	 public static int[] liste = { 5, 8, 11, 2, 20, 4, 9, 1, 17, 13, 12, 18, 3, 19,10,6,14,7,16 };

	    public int[] sortieren(int erstens, int zweitens) {
	        int q;
	        if (erstens < zweitens) {
	            q = partition(erstens, zweitens);
	            sortieren(erstens, q);
	            sortieren(q + 1, zweitens);
	        }
	        return liste;
	    }

	    int partition(int erstens, int zweitens) {
	        int h�lfte2, h�lfte1, x = liste[(erstens + zweitens) / 2];
	        h�lfte2 = erstens - 1;
	        h�lfte1 = zweitens + 1;
	        
	        do {
	            h�lfte2++;
	        } while (liste[h�lfte2] < x);

	        do {
	        	h�lfte1--;
	        } while (liste[h�lfte1] > x);

	        if (h�lfte2 < h�lfte1) {
	        	System.out.println(liste[h�lfte2] + " und " + liste[h�lfte1] + " werden getauscht.");
	            int k = liste[h�lfte2];
	            liste[h�lfte2] = liste[h�lfte1];
	            liste[h�lfte1] = k;
	        } else {
	            return h�lfte1;
	        }
	        return -1;
	    }

	    public static void main(String[] args) {
	        Quicksort qs1 = new Quicksort();
	        Stoppuhr s1 = new Stoppuhr();
	        s1.starten();
	        int[] arr = qs1.sortieren(0, liste.length - 1);
	        s1.stoppen();
	        System.out.println(s1.getDauer() + " Millisekunden hat das sortieren gedauert.");
	       
	    } 
}
