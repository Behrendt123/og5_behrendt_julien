package addon;

public class Addon {

	private int Verkaufspreis;
	private String Bezeichnung;
	private int IDNummer;
	private int MaxAnzahl;
	private int Anzahl;
	
	public Addon(){
		this.Verkaufspreis = 0;//In Cents
		this.Bezeichnung = "";
		this.IDNummer = 0;
		this.MaxAnzahl = 0;
		this.Anzahl = 0;	
	}
	
	public Addon (int Verkaufspreis ,String Bezeichnung ,int IDNummer ,int MaxAnzahl  ,int Anzahl  ) {
		this.Verkaufspreis = Verkaufspreis;//In Cents
		this.Bezeichnung = Bezeichnung;
		this.IDNummer = IDNummer;
		this.MaxAnzahl = MaxAnzahl;
		this.Anzahl = Anzahl;
	}
	
	

	public int getVerkaufspreis() {
		return Verkaufspreis;
	}

	public void setVerkaufspreis(int verkaufspreis) {
		Verkaufspreis = verkaufspreis;
	}

	public String getBezeichnung() {
		return Bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		Bezeichnung = bezeichnung;
	}

	public int getIDNummer() {
		return IDNummer;
	}

	public void setIDNummer(int iDNummer) {
		IDNummer = iDNummer;
	}

	public int getMaxAnzahl() {
		return MaxAnzahl;
	}

	public void setMaxAnzahl(int maxAnzahl) {
		MaxAnzahl = maxAnzahl;
	}

	public int getAnzahl() {
		return Anzahl;
	}

	public void setAnzahl(int anzahl) {
		Anzahl = anzahl;
	}
	
	public void verbrauchAddon(int Anzahl){
		if(this.Anzahl>0){
			this.Anzahl= this.Anzahl-Anzahl;
		}
	}
	
	public void erwerbAddon(int Anzahl){
		if (this.Anzahl<MaxAnzahl){
			this.Anzahl = this.Anzahl + Anzahl;
		}
	}
	
	public int gesamtwertBerechnen(){
		int Gesamtwert = Anzahl*Verkaufspreis;
		return Gesamtwert;
		}
}
