
public class KeyStore01 extends java.lang.Object{
	
	private String[] key;
	private int currentPos;

	
	public KeyStore01(){
		key = new String[100];
		
	}
	public KeyStore01(int lenght){
		key = new String[lenght];
	}
	
	public java.lang.String get(int index){
		return key[index];
	}
	
	public boolean add(String e){
		if(currentPos >= key.length)
			return false;
		else{
			key[currentPos]= e;
			this.currentPos ++;
			return true;
		}
	}
	
	public int size(){
		return currentPos;
	}
	
	public void clear(){
		for(int i = currentPos;i>0;i--){
			key[i]=null;
			this.currentPos--;
		}
			
	}
	
	public boolean remove(int index){
		if(currentPos<index){
			return false;
		}
		else{
			key[index]=null;
			for(int i = index + 1; i<currentPos;i++){
				key[i-1] = key[i];
				key[i]= null;
			}
			this.currentPos --;
			return true;
		}
	}
	
	public boolean remove(java.lang.String str){
		for(int i =0; i<currentPos;i++){
			if(key[i].equals(str)){
				key[i]= null;
				for(int j = i + 1;j<currentPos;j++){
					key[j-1]=key[j];
					key[j]= null;
				}
				this.currentPos --;
				return true;
			}
		}
		return false;
	}
	
	public int indexOf(java.lang.String str){
		for(int i = 0;i<currentPos;i++){
			if(key[i].equals(str)){
				return i;
			}
		}
		return -1;
		
	}
	
	public java.lang.String toString(){
		String anzeige="";
		for(int i = 0;i<currentPos;i++){
			anzeige = anzeige + " " + key[i];
		}
		return anzeige;
	}
}
