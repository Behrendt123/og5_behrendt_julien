import java.util.ArrayList;
public class KeyStore02 {
	private ArrayList<String> keylist;
	
	
	public KeyStore02(){
		keylist=new ArrayList<String>();
	}
	
	public java.lang.String get(int index){
		return keylist.get(index);
	}
	
	public void clear(){
		keylist.clear();
	}
	
	public int size(){
		return keylist.size();
	}
	
	public boolean remove(int index){
		if(index<= keylist.size()){
			keylist.remove(index);
			for(int i = index;i<keylist.size();i++){
				keylist.set(i, keylist.get(i+1));
			}
			return true;		}
		return false;
	}
	
	public boolean remove(java.lang.String str){
		if(keylist.contains(str)){
			keylist.remove(str);
			for(int i = keylist.indexOf(str);i<keylist.size();i++){
				keylist.set(i,keylist.get(i+1));
			}
			return true;
		}
		return false;
	}
	
	public int indexOf(java.lang.String str){
		return keylist.indexOf(str);
	}
	
	public java.lang.String toString(){
		return keylist.toString();
	}
	
	public boolean add(String e){
	keylist.add(e);
	return true;
	}
}
