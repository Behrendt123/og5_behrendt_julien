

import java.util.List;

public class LearnerData {
	private String learnerName;
	private List<Lerneintrag> listEntries;
	
	public LearnerData(String name, List<Lerneintrag> list) {
		this.setLearnerName(name);
		this.setListEntries(list);
	}
	
	public String getLearnerName(){
		return learnerName;
	}
	public void setLearnerName(String learnerName){
		this.learnerName = learnerName;
	}
	
	public List<Lerneintrag> getListEntries(){
		return listEntries;
	}
	
	public void setListEntries(List<Lerneintrag> listEntries) {
		this.listEntries = listEntries;
	}
}
