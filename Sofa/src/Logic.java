import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class Logic{
	
	
	private List<Lerneintrag> listEntries;
	private String learnerName;
	private final FileControl FILE_CONTROL;
	
	
	public Logic(FileControl fc){
		this.FILE_CONTROL = fc;
		reloadFromDisk();
	}
	
	

	public List <Lerneintrag> getListEntries(){
		return listEntries;
	}
	
	public String getLearnerName(){
		return learnerName;
	}
	public void setListEntries(List<Lerneintrag> listEntries){
		this.listEntries = listEntries;
	}
	public void reloadFromDisk(){
		LearnerData ldata = FILE_CONTROL.getFileContent();
		this.learnerName = ldata.getLearnerName();
		this.listEntries = ldata.getListEntries();
	}
	
	public boolean addEntry(Lerneintrag l) {
		if (FILE_CONTROL.addEntry(l)){
			listEntries.add(l);
			return true;
		}
		return false;
	}
	public String createReport(){
		return "";
	}
	
		
	}


	
