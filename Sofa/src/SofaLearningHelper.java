 

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SofaLearningHelper {
	public static Date stringToDate(String s) {
		DateFormat df = new SimpleDateFormat("dd.mm.yyyy");
		Date date = null;
		try{
			date = df.parse(s);
		}catch (ParseException e){
			System.err.printf("Fehlerhaftes Datum '%s'", s);
		}
		return date;
	}
	
	public static String dateToString(Date d){
		DateFormat df = new SimpleDateFormat("dd.mm.yyyy");
		return df.format(d);
	}
}
