import java.util.Date;
public class Lerneintrag {
	private Date Datum;
	private String fach;
	private String Beschreibung;
	private int Dauer;
	
	public Lerneintrag(Date Datum,String fach,String Beschreibung,int Dauer){
		this.Datum=Datum;
		this.fach=fach;
		this.Beschreibung=Beschreibung;
		this.Dauer=Dauer;
	}
	public String getFach() {
		return fach;
	}
	public void setFach(String fach) {
		this.fach = fach;
	}
	public String getBeschreibung() {
		return Beschreibung;
	}
	public void setBeschreibung(String beschreibung) {
		Beschreibung = beschreibung;
	}
	public int getDauer() {
		return Dauer;
	}
	public void setDauer(int dauer) {
		Dauer = dauer;
	}
	
	public String toString(){
		String s = Datum + " " + fach + " " + Beschreibung + " " + Dauer;
		return s ;
	}
	public Date getDatum() {
		return Datum;
	}
	public void setDatum(Date datum) {
		Datum = datum;
	}
}
//String meinString = "16.01.2017";
//SimpleDateFormat meinDatumsformat = new SimpleDateFormat("dd.mm.yyyy");
//Date meinDatum = meinDatumsformat.parse(meinString);