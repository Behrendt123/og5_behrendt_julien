import java.io.*;
import java.util.ArrayList;
import java.util.Date;



public class FileControl {
	
private String filename;
	
	public FileControl(String filename){
		this.filename = filename;
	}
	
	public LearnerData getFileContent(){
		ArrayList<Lerneintrag> list = new ArrayList<Lerneintrag>();
		LearnerData ldata = new LearnerData("unbekannt", list);
		FileReader fr;
		BufferedReader br;
		
		try {
			File f = new File(filename);
			System.out.format("Lese Datei %s ein...%n", f.getAbsoluteFile());
			fr = new FileReader(f);
			br = new BufferedReader(fr);
			String s = br.readLine();
			ldata.setLearnerName(s);
			s = br.readLine();
			while(s != null){
				Date datum = SofaLearningHelper.stringToDate(s);
				String fach = br.readLine();
				String inhalt = br.readLine();
				int dauer = Integer.parseInt(br.readLine());
				list.add(new Lerneintrag (datum, fach, inhalt, dauer));
				s = br.readLine();
				
			}
			br.close();
		}catch (Exception e){
			System.out.println("Fehler beim Einlesen der Datei " + filename);
			e.printStackTrace();
		}
		return ldata;
	}
	public boolean addEntry(Lerneintrag eintrag){
		PrintWriter pWriter = null;
		try{
			pWriter = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)));
			pWriter.println(SofaLearningHelper.dateToString(eintrag.getDatum()));
			pWriter.println(eintrag.getFach());
			pWriter.println(eintrag.getBeschreibung());
			pWriter.println(eintrag.getDauer());
		}catch (IOException ioe){
			ioe.printStackTrace();
			return false;
		}finally {
			if (pWriter != null) {
				pWriter.flush();
				pWriter.close();
			}
		}
		return true;
	}
}